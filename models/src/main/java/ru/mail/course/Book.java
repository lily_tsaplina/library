package ru.mail.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public final class Book {
    private long id;
    @NotNull
    private Author author;
    @NotNull
    private String name;
    private int publicationYear;
}
