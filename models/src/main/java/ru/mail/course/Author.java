package ru.mail.course;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public final class Author {
    private long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
}
