package ru.mail.course;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public final class LibraryInMemory implements Library {
    @NotNull
    private Set<Book> books = new HashSet<>();

    @Override
    public boolean addBook(@NotNull Book book) {
        return books.add(book);
    }

    @Override
    public boolean addAllBooks(@NotNull Collection<Book> books) {
        return this.books.addAll(books);
    }

    @NotNull
    @Override
    public Set<Book> findByAuthorName(@NotNull String firstName, @NotNull String lastName) {
        return books.stream()
                .filter(book -> book.getAuthor().getFirstName().equalsIgnoreCase(firstName)
                            && book.getAuthor().getLastName().equalsIgnoreCase(lastName))
                .collect(Collectors.toSet());
    }

    @NotNull
    @Override
    public Set<Book> findAll() {
        return this.books;
    }
}
