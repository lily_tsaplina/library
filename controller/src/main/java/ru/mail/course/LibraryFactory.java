package ru.mail.course;

import org.jetbrains.annotations.NotNull;

public final class LibraryFactory {
    @NotNull
    public static Library getLibrary() {
        return new LibraryInMemory();
    }
}
