package ru.mail.course;

import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public final class Application {
    public static void main(@Nullable String[] args) {
        Library library = LibraryFactory.getLibrary();
        fillLibraryWithMocks(library);
        Gson gson = new Gson();

        try (Scanner in = new Scanner(System.in)) {
            String reply;
            do {
                System.out.println("Enter author first name: ");
                String authorFirstName = in.nextLine().trim();
                System.out.println("Enter author last name: ");
                String authorLastName = in.nextLine().trim();

                Set<Book> books = library.findByAuthorName(authorFirstName, authorLastName);
                if (books.size() == 0) {
                    System.out.println("Books written by this author were not found. ");
                } else {
                    for (Book book : books) {
                        System.out.println(gson.toJson(book));
                    }
                }

                do {
                    System.out.println("Continue (y/n)? ");
                    reply = in.nextLine().trim();
                } while (!(reply.equals("y") || reply.equals("n")));
            } while (reply.equals("y"));
        }
    }

    private static void fillLibraryWithMocks(@NotNull Library library) {
        Author jane = new Author(0, "Jane", "Austen");
        Author charles = new Author(1, "Charles", "Dickens");

        Set<Book> books = new HashSet<Book>() {{
            add(new Book(0, jane, "Pride and Prejudice", 1999));
            add(new Book(0, jane, "Emma", 1990));
            add(new Book(1, charles, "A Tale of Two Cities", 1968));
            add(new Book(1, charles, "Great Expectations", 1985));
        }};

        library.addAllBooks(books);
    }
}
