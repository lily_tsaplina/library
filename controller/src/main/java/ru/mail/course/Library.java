package ru.mail.course;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Set;

public interface Library {

    boolean addBook(@NotNull Book book);

    boolean addAllBooks(@NotNull Collection<Book> books);

    @NotNull
    Set<Book> findByAuthorName(@NotNull String firstName, @NotNull String lastName);

    @NotNull
    Set<Book> findAll();
}
